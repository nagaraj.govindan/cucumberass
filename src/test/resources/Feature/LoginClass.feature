
Feature: Demo web shop validation

Scenario Outline: To validate the demo web shop login functionality
  Given open the website
  And The homepage of the website get displayed
	When Click on the login profile button
	And Use should enter "<USERNAME>" the valid Username
	And User enter "<PASSWORD>" the valid Password
	Then Click on the login button
	Then The user should taken to the homepage of the demo web shop
	And close the browser
	
	   Examples: 
      |         USERNAME          | PASSWORD    |
      |  n95000030320@gmail.com   | Naga@5565   |
      |  n95000030320@gmail.com   | Naga@5565  |
	